# frozen_string_literal: true

php_version = ENV.fetch('PHP_VERSION', '7.4.8')

name "php-#{php_version}"
maintainer "GitLab B.V."
homepage "https://www.php.net/"

install_dir "/opt/asdf/installs/php/#{php_version}"
package_scripts_path Pathname.pwd.join("config/scripts/php")

build_version php_version
build_iteration 1

override 'asdf_php', version: php_version
dependency "asdf_php"

package :deb do
  compression_level 9
  compression_type :xz
end
