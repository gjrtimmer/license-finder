# frozen_string_literal: true

require_relative '../../lib/license/management/version.rb'
name "license_management"

default_version License::Management::VERSION
source({
  path: Dir.pwd,
  options: {
    exclude: %w[bin coverage pkg spec tmp vendor .git .gitlab]
  }
})

dependency "libxml2"
dependency "ruby"

build do
  touch "#{install_dir}/bin/.keep"
  touch "#{install_dir}/.local/bin/.keep"

  gem "build #{project_dir}/license-management.gemspec -o #{project_dir}/release.gem"
  gem "install -f #{project_dir}/release.gem --no-document"
  delete "#{project_dir}/release.gem"
  copy "#{install_dir}/embedded/bin/license_management", "#{install_dir}/bin/license_management"
  command "cp -r #{project_dir}/config/files/. #{install_dir}/"
end

build do
  command "mvn license:license-list"
  command [
    :mvn,
    "dependency:get",
    "-Dartifact=gradle.plugin.com.hierynomus.gradle.plugins:license-gradle-plugin:0.15.0",
    "-DremoteRepositories=https://plugins.gradle.org/m2"
  ].map(&:to_s).join(' ')
  command "mvn dependency:get -Dartifact=org.codehaus.plexus:plexus-utils:2.0.6"
  copy "#{Dir.home}/.m2/repository/", "#{install_dir}/.m2/repository/"
end

build do
  touch "#{install_dir}/.config/virtualenv/app-data/.keep"
  command "pip2 download -d #{install_dir}/.config/virtualenv/app-data pip-licenses setuptools wheel"
  command "pip3 download -d #{install_dir}/.config/virtualenv/app-data pip-licenses setuptools wheel"
end

build do
  delete "#{install_dir}/embedded/lib/ruby/gems/**/cache"
  delete "#{install_dir}/embedded/lib/ruby/gems/**/doc"
  delete "#{install_dir}/embedded/lib/ruby/gems/**/build_info"
  command "find #{install_dir} -type f -name '*.png' -exec rm {} +"
  command "find #{install_dir} -type f -name '*.ttf' -exec rm {} +"
  command "find #{install_dir} -type f -name '*.rhtml' -exec rm {} +"
  command "find #{install_dir} -type f -name '*.js' -exec rm {} +"
end
