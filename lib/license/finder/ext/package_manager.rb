# frozen_string_literal: true

module LicenseFinder
  class PackageManager
    def shell
      ::License::Management.shell
    end

    def log
      ::License::Management.logger
    end

    def tool_box
      @tool_box ||= ::License::Management::ToolBox.new(shell, project_path)
    end

    def prepare
      create_vendor_path
      return unless prepare_command

      within_project_path do
        shell.execute(prepare_command, capture: false)
      end
    end

    def current_packages_with_relations
      current_packages
    rescue StandardError => e
      log.error(e)
      raise e unless @prepare_no_fail

      []
    end

    protected

    def vendor_path
      @vendor_path ||= Pathname.pwd.join('.gitlab', 'cache', 'vendor')
    end

    def create_vendor_path
      shell.execute([:mkdir, '-p', vendor_path], capture: false) unless vendor_path.exist?
    end

    def within_project_path
      Dir.chdir(project_path) { yield }
    end
  end
end
